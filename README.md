# Introduction au Deep Learning

## Getting started

Lien du notebook Jupyter sur Google Colab : https://colab.research.google.com/drive/1wTNXc3RsSCqOmhATTWkyLzzjw9w8pGUf?usp=sharing

Une fois que vous êtes dessus, allez dans "Fichier", et "Enregistrer une copie dans Drive" pour avoir une copie du notebook que vous pourrez conserver et modifier comme bon vous semble. 
La copie est généralement située dans un dossier 'Colab Notebooks', dans votre Drive Google. 

Ce repository contient trois dossiers :

- "CatsAndDogs" qui contient des images issues d'un dataset de chien et cha, téléchargé à l'adresse suivante : https://www.kaggle.com/datasets/shaunthesheep/microsoft-catsvsdogs-dataset 
- "Fish Dataset" qui contient des images issues d'un dataset de poissons, 2 parmi 9 espèces de poissons consommées dans la région Égéenne en Turquie.Elles sont issues de la publication suivante : https://ieeexplore.ieee.org/document/9259867  
- "Others" qui contient des fichiers divers, comme des poids de réseau pré-entrainé, des métriques ou encore un graphique d'apprentissage.


## Contact

gino.frazzoli@umontpellier.fr

isdm-clinique@umontpellier.fr

## Author

Gino Frazzoli, Institut de Science des Données de Montpellier (ISDM)
